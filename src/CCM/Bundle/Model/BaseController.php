<?php
namespace CCM\Bundle\Model;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    /**
     * Renders the error page with the given error
     *
     * @param string $error The error message
     *
     * @return render
     */
    protected function renderError($error)
    {
        return $this->render('CCMBundle:Info:error.html.twig',
                             array("error" => $error)
                        );
    }

    /**
     * Renders the info page with the given information
     *
     * @param string $info The info message
     *
     * @return render
     */
    protected function renderInfo($info)
    {
        return $this->render('CCMBundle:Info:info.html.twig',
                             array("info" => $info)
                        );
    }

    /**
     * Renders the success page with the given success message
     *
     * @param string $message The message
     *
     * @return render
     */
    protected function renderSuccess($message)
    {
        return $this->render('CCMBundle:Info:success.html.twig',
                             array("message" => $message)
                        );
    }
}

?>
