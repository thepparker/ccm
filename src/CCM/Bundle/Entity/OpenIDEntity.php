<?php
// src/CCM/Bundle/Entity/OpenIDEntity.php 
namespace CCM\Bundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

use Fp\OpenIdBundle\Entity\UserIdentity as BaseUserIdentity;
use Fp\OpenIdBundle\Model\UserIdentityInterface;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="openid_identity")
 */
class OpenIDEntity extends BaseUserIdentity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id; //primary key

    // This class inherits an identity string and an attributes array
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
