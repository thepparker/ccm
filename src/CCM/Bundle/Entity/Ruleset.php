<?php
namespace CCM\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CCM\Bundle\Entity\Repository\RuleSetRepository")
 * @ORM\Table(name="rulesets")
 */
class Ruleset
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @ORM\Column(type="text")
     */
    protected $value;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Ruleset
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set val
     *
     * @param string $val
     * @return Ruleset
     */
    public function setValue($val)
    {
        $this->value = $val;

        return $this;
    }

    /**
     * Get val
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
}
