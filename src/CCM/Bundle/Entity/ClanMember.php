<?php

/**
 * A ClanMember entity is a single member of a clan, and refers to a single
 * User. Many ClanMember entities can refer to a single user. One user has
 * one ClanMember entity per clan that the user is a member of.
 *
 */

namespace CCM\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="clan_memberships")
 */
class ClanMember 
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="clanMemberships")
     */
    protected $user;

    /**
     * Whether or not this member is a leader of the clan this member
     * belongs to.
     *
     * @ORM\Column(type="boolean")
     */
    protected $leader;

    /**
     * Whether or not this member is the owner of this clan
     * 
     * @ORM\Column(type="boolean")
     */
    protected $owner;

    /**
     * The clan
     *
     * @ORM\ManyToOne(targetEntity="Clan", inversedBy="members")
     */
    protected $clan;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set leader
     *
     * @param boolean $leader
     * @return ClanMember
     */
    public function setLeader($leader)
    {
        $this->leader = $leader;

        return $this;
    }

    /**
     * Is user a leader of clan?
     *
     * @return boolean 
     */
    public function isLeader()
    {
        return $this->leader;
    }

    /**
     * Set owner
     *
     * @param boolean $owner
     * @return ClanMember
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Is user owner of clan?
     *
     * @return boolean 
     */
    public function isOwner()
    {
        return $this->owner;
    }

    /**
     * Set user
     *
     * @param \CCM\Bundle\Entity\User $user
     * @return ClanMember
     */
    public function setUser(\CCM\Bundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CCM\Bundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set clan
     *
     * @param \CCM\Bundle\Entity\Clan $clan
     * @return ClanMember
     */
    public function setClan(\CCM\Bundle\Entity\Clan $clan = null)
    {
        $this->clan = $clan;

        return $this;
    }

    /**
     * Get clan
     *
     * @return \CCM\Bundle\Entity\Clan 
     */
    public function getClan()
    {
        return $this->clan;
    }
}
