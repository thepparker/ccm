<?php
/**
 * A division is a group of clans in a league. Leagues may consist of any
 * number of divisions. Divisions are the only entity that contain references
 * to the clans in each league, the league entity holds no record of the clans
 * in it. Each division also keeps track of the league it belongs to.
 *
 * A division can only belong to one league. New leagues contain new divisions.
 * Divisions can be added to leagues that have not begun.
 */

namespace CCM\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CCM\Bundle\Entity\Repository\DivisionRepository")
 * @ORM\Table(name="divisions")
 */
class Division
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", unique=true)
     */
    protected $name;

    /**
     * The league this Division belongs to
     *
     * @ORM\Column(type="integer")
     */
    protected $league;

    /**
     * The clans in this division and their rankings
     *
     * @ORM\Column(type="array")
     */
    protected $clans;

    /**
     * The admins of this division
     *
     * @ORM\Column(type="array")
     */
    protected $admins;

    public function __construct()
    {
        $this->admins = array();
        $this->clans = array();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Division
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set league
     *
     * @param integer $league
     * @return Division
     */
    public function setLeague($league)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return integer
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * Set clans
     *
     * @param array $clans
     * @return Division
     */
    public function setClans(array $clans)
    {
        $this->clans = array();

        foreach ($clans as $clan)
        {
            $this->addClan($clan);
        }

        return $this;
    }

    public function addClan($clan)
    {
        if (!in_array($clan, $this->getClans()))
        {
            $this->clans[] = $clan;
        }

        return $this;
    }

    /**
     * Get clans
     *
     * @return array
     */
    public function getClans()
    {
        return $this->clans;
    }

    /**
     * Set admins
     *
     * @param array $admins
     * @return Division
     */
    public function setAdmins(array $admins)
    {
        $this->admins = array();

        foreach ($admins as $admin)
        {
            $this->addAdmin($admin);
        }

        return $this;
    }

    public function addAdmin($admin)
    {
        if (!in_array($admin, $this->getAdmins()))
        {
            $this->admins[]  = $admin;
        }

        return $this;
    }

    /**
     * Get admins
     *
     * @return array
     */
    public function getAdmins()
    {
        return $this->admins;
    }
}
?>
