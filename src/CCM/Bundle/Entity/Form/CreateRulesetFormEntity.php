<?php
namespace CCM\Bundle\Entity\Form;

class CreateRulesetFormEntity
{
    protected $name;

    protected $value;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}
?>
