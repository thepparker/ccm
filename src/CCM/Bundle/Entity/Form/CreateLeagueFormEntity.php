<?php
namespace CCM\Bundle\Entity\Form;

use Doctrine\Common\Collections\ArrayCollection;

class CreateLeagueFormEntity
{
    protected $name;

    protected $divisions;

    protected $ruleset;

    protected $admins;

    public function __construct()
    {
        $this->setName("New league name");

        $this->divisions = new ArrayCollection();
        $this->admins = new ArrayCollection();
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDivisions($divisions)
    {
        $this->divisions = $divisions;

        return $this;
    }

    public function addDivision($division)
    {
        $this->divisions->add($division);

        return $this;
    }

    public function removeDivision($division)
    {
        $this->divisions->removeElement($division);

        return $this;
    }

    public function getDivisions()
    {
        return $this->divisions;
    }

    public function setAdmins($admins)
    {
        $this->admins = $admins;

        return $this;
    }

    public function addAdmin($admin)
    {
        $this->admins->add($admin);

        return $this;
    }

    public function removeAdmin($admin)
    {
        $this->admins->remove($admin);

        return $this;
    }

    public function getAdmins()
    {
        return $this->admins;
    }

    public function setRuleset($ruleset)
    {
        $this->ruleset = $ruleset;

        return $this;
    }

    public function getRuleset()
    {
        return $this->ruleset;
    }
}
?>
