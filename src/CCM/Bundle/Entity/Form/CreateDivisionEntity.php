<?php
/**
 * A division is a group of clans in a league. Leagues may consist of any
 * number of divisions. Divisions are the only entity that contain references
 * to the clans in each league, the league entity holds no record of the clans
 * in it. Each division also keeps track of the league it belongs to.
 *
 * A division can only belong to one league. New leagues contain new divisions.
 * Divisions can be added to leagues that have not begun.
 */

namespace CCM\Bundle\Entity\Form;

class CreateDivisionEntity
{
    protected $name;

    protected $clans;

    protected $admins;

    public function __construct()
    {
        $this->clans = array();
        $this->admins = array();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Division
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set clans
     *
     * @param array $clans
     * @return Division
     */
    public function setClans($clans)
    {
        $this->clans = $clans;

        return $this;
    }

    /**
     * Get clans
     *
     * @return array
     */
    public function getClans()
    {
        return $this->clans;
    }

    /**
     * Set admins
     *
     * @param array $admins
     * @return Division
     */
    public function setAdmins($admins)
    {
        $this->admins = $admins;

        return $this;
    }

    /**
     * Get admins
     *
     * @return array
     */
    public function getAdmins()
    {
        return $this->admins;
    }
}
?>
