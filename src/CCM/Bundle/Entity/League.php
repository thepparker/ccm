<?php
/**
 * A league is just considered a group of divisions under
 * a single ruleset and name. No clan information is stored in this entity,
 * only which divisions are in this league. Each division has a listing of 
 * each clan and their standing in the division.
 */

namespace CCM\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use CCM\Bundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="CCM\Bundle\Entity\Repository\LeagueRepository")
 * @ORM\Table(name="leagues")
 */
class League
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $ruleset;

    /**
     * @ORM\Column(type="array")
     */
    protected $divisions;

    /**
     * The admins of this league
     *
     * @ORM\Column(type="array")
     */
    protected $admins;

    public function __construct()
    {
        $this->divisions = array();
        $this->admins = array();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return League
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ruleset
     *
     * @param integer $ruleset
     * @return League
     */
    public function setRuleset($ruleset)
    {
        $this->ruleset = $ruleset;

        return $this;
    }

    /**
     * Get ruleset
     *
     * @return integer 
     */
    public function getRuleset()
    {
        return $this->ruleset;
    }

    /**
     * Set divisions
     *
     * @param array $divisions
     * @return League
     */
    public function setDivisions($divisions)
    {
        $this->divisions = $divisions;

        return $this;
    }

    /**
     * Get divisions
     *
     * @return array 
     */
    public function getDivisions()
    {
        return $this->divisions;
    }

    /**
     * Set admins
     *
     * @param array $admins
     * @return League
     */
    public function setAdmins($admins)
    {
        $this->admins = array();
        foreach ($admins as $admin)
        {
            $this->addAdmin($admin);
        }

        return $this;
    }

    public function addAdmin($admin)
    {
        if (!in_array($admin, $this->admins))
        {
            $this->admins[] = $admin;
        }

        return $this;
    }

    /**
     * Get admins
     *
     * @return array 
     */
    public function getAdmins()
    {
        return $this->admins;
    }

    /**
     * Check if the given user is an admin of this league
     *
     * @param User $user The user
     *
     * @return boolean
     */
    public function isAdmin(User $user)
    {
        return in_array($user->getId(), $this->getAdmins());
    }
}
