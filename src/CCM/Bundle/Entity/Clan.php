<?php
namespace CCM\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="CCM\Bundle\Entity\Repository\ClanRepository")
 * @ORM\Table(name="clans")
 */
class Clan
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=8, unique=true)
     */
    protected $tag;

    /**
     * @ORM\OneToMany(targetEntity="ClanMember", mappedBy="clan")
     */
    protected $members;

    /**
     * @ORM\ManyToMany(targetEntity="League")
     */
    protected $leagues;

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->leagues = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setLeagues(ArrayCollection $leagues)
    {
        $this->leagues = $leagues;

        return $this;
    }

    public function addLeague($league)
    {
        $this->leagues->add($league);

        return $this;
    }

    public function getLeagues()
    {
        return $this->leagues;
    }

    /**
     * @param string $tag
     *
     * @return CCM\Bundle\Entity\Clan
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $name
     *
     * @return CCM\Bundle\Entity\Clan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param ArrayCollection $members
     *
     * @return CCM\Bundle\Entity\Clan
     */
    public function setMembers(ArrayCollection $members)
    {
        $this->members = $members;

        return $this;
    }

    /**
     * @param ClanMember $member
     *
     * @return CCM\Bundle\Entity\Clan
     */
    public function addMember(ClanMember $member)
    {
        $this->members->add($member);

        return $this;
    }

    /**
     * @return array
     */
    public function getMembers()
    {
        return $this->members;
    }
}

?>
