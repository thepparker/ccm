<?php
namespace CCM\Bundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

use Doctrine\ORM\NoResultException;

class DivisionRepository extends EntityRepository
{
    /**
     * @param integer $id Division ID
     *
     * @return Division
     */
    public function findOneById($id)
    {
        $qb = $this->getQb();

        $qb->where('d.id = :id')
           ->setParameter('id', $id);

        return $this->singleQueryResult($qb);
    }

    /**
     * @param array $names An array of names
     *
     * @return array An array of divisions
     */
    public function findManyByName(array $names)
    {
        if (empty($names))
        {
            return array();
        }

        $qb = $this->getQb();

        $qb->where($qb->expr()->in('d.name', $names));

        return $qb->getQuery()->getResult();
    }

    /**
     * Get a number of Divisions based on ID
     *
     * @param array $ids Division ids
     *
     * @return array
     */
    public function findManyById(array $ids)
    {
        if (empty($ids))
        {
            return array();
        }

        $qb = $this->getQb();
        $qb->where($qb->expr()->in('d.id', $ids));

        return $qb->getQuery->getResult();
    }

    private function getQb()
    {
        return $this->createQueryBuilder('d');
    }

    /**
     * This function gets a single result from a query builder object
     * or null if there are no matching results
     *
     * @param QueryBuilder $qb A query builder
     *
     * @return CCM\Bundle\Entity\User or null
     */
    protected function singleQueryResult($qb)
    {
        try
        {
            $res = $qb->getQuery()->getSingleResult();
        }
        catch (NoResultException $e)
        {
            $res = null;
        }

        return $res;
    }
}

?>
