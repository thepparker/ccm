<?php
/**
 * This custom repository provides useful methods for getting clans
 * that avoids the use of the default findBy, as it is inefficient
 */
namespace CCM\Bundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

use Doctrine\ORM\NoResultException;

class ClanRepository extends EntityRepository
{
    /**
     * @param integer $id The clan id
     *
     * @return CCM\Bundle\Entity\Clan
     */
    public function findOneById($id)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->where('c.id = :id')
           ->setParameter('id', $id);

        return $this->singleQueryResult($qb);
    }

    /**
     * @param string $name The clan name
     * @param string $tag The clan tag
     *
     * @return CCM\Bundle\Entity\Clan
     */
    public function findByNameOrTag($name = null, $tag = null)
    {
        if ($name === null && $tag === null)
        {
            return null;
        }

        $qb = $this->createQueryBuilder('c');

        // if the name is null, we select by tag
        // if the tag is null, we select by name
        // if neither are null, both are valid
        if ($name === null)
        {
            $qb->where('c.tag = :tag')
               ->setParameter('tag', $tag);
        }
        else if ($tag === null)
        {
            $qb->where('c.name = :name')
               ->setParameter('name', $name);
        }
        else
        {
            $qb->where('c.name = :name')
               ->orWhere('c.tag = :tag')
               ->setParameters(array(
                    'name' => $name,
                    'tag' => $tag
                ));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $clans An array of clan IDs
     *
     * @return array An array of CLan objects
     */
    public function findManyByIds(array $clans)
    {
        if (empty($clans))
        {
            return array();
        }

        $qb = $this->createQueryBuilder('c');

        $qb->where($qb->expr()->in('c.id', $clans));

        return $qb->getQuery()->getResult();
    }

    /**
     * Get many clans based on the given name array
     *
     * @param array $names Clan names
     *
     * @return array
     */
    public function findManyByName(array $names)
    {
        if (empty($names))
        {
            return array();
        }

        $qb = $this->createQueryBuilder('c');
        $qb->where($qb->expr()->in('c.name', $names));

        return $qb->getQuery()->getResult();
    }

    /**
     * This function gets a single result from a query builder object
     * or null if there are no matching results
     *
     * @param QueryBuilder $qb A query builder
     *
     * @return CCM\Bundle\Entity\Clan or null
     */
    protected function singleQueryResult($qb)
    {
        try
        {
            $res = $qb->getQuery()->getSingleResult();
        }
        catch (NoResultException $e)
        {
            $res = null;
        }

        return $res;
    }
}

?>
