<?php
namespace CCM\Bundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

use Doctrine\ORM\NoResultException;

class UserRepository extends EntityRepository
{
    /**
     * @param bigint $sid User's 64-bit steam id
     *
     * @return CCM\Bundle\Entity\User
     */
    public function findBySteamId($sid)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->where('u.steamid = :steamid')
           ->setParameter('steamid', $sid);

        return $this->singleQueryResult($qb);
    }

    /**
     * @param integer $id User's id
     *
     * @return CCM\Bundle\Entity\User
     */
    public function findById($id)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->where('u.id = :id')
           ->setParameter('id', $id);

        return $this->singleQueryResult($qb);
    }

    /**
     * @param array $names An array of names
     *
     * @return array An array of users
     */
    public function findManyByName(array $names)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->where($qb->expr()->in('u.nickname', $names));

        return $qb->getQuery()->getResult();
    }

    /**
     * This function gets a single result from a query builder object
     * or null if there are no matching results
     *
     * @param QueryBuilder $qb A query builder
     *
     * @return CCM\Bundle\Entity\User or null
     */
    protected function singleQueryResult($qb)
    {
        try
        {
            $res = $qb->getQuery()->getSingleResult();
        }
        catch (NoResultException $e)
        {
            $res = null;
        }

        return $res;
    }
}

?>
