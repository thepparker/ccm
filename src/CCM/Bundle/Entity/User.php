<?php
// src/CCM/Bundle/Entity/User.php
namespace CCM\Bundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="CCM\Bundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Usernames are _NOT_ explicitly used by this application, but they are
     * required for ACLs to function correctly. This means that usernames
     * _MUST_ be unique. The only unique attributes for users are steamid
     * and user id. We use steamids as the 'username'.
     *
     * @var string username
     */
    protected $username;

    /**
     * User alias. This is the user's name.
     *
     * @ORM\Column(type="text")
     */
    protected $nickname;

    /**
     * @ORM\Column(type="bigint", unique=true)
     */
    protected $steamid;

    /**
     * @ORM\Column(type="array")
     */
    protected $roles;

    /**
     * A user can be in many clans. One user has a relationship
     * to many ClanMember entities. Each clanMembership is a
     * clanMember entity, and represents that user's role in the given
     * clan
     *
     * @ORM\OneToMany(targetEntity="ClanMember", mappedBy="user")
     */
    protected $clanMemberships;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $admin;

    // must implement these vars, but they are not used
    protected $password;
    protected $salt;

    // begin functions
    public function __construct()
    {
        $this->roles = array();
        $this->clanMemberships = new ArrayCollection();

        $this->admin = false;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * getUsername is just a mask for steamid, as we use steamids for unique
     * identifiers.
     *
     * @return string steamid
     */
    public function getUsername()
    {
        return $this->getSteamid();
    }

    public function setNickname($name)
    {
        $this->nickname = $name;
        return $this;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param long $steamid
     *
     * @return CCM\Bundle\Entity\User
     */
    public function setSteamid($steamid)
    {
        $this->steamid = $steamid;

        return $this;
    }

    /**
     * @return long steamid
     */
    public function getSteamid()
    {
        return $this->steamid;
    }

    /**
     * @param string $role
     *
     * @return CCM\Bundle\Entity\User
     */
    public function addRole($role)
    {
        $role = strtoupper($role);

        // don't add ROLE_USER to the role array, because it is
        // appended as a default role anyway
        if ($role === 'ROLE_USER')
        {
            return $this;
        }

        // else, add the role if it isn't already present
        if (!in_array($role, $this->roles, true))
        {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @param array $roles
     *
     * @return CCM\Bundle\Entity\User
     */
    public function setRoles(array $roles)
    {
        $this->roles = array();

        // iterate over the roles and add using addRole so we can be
        // sure that we don't get any unwanted roles in the role array
        foreach ($roles as $role)
        {
            $this->addRole($role);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->roles;
        
        // we always append ROLE_USER as a default role
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function hasRole($role)
    {
        return in_array($role, $this->getRoles());
    }

    /** 
     * @param ArrayCollection $memberships
     *
     * @return CCM\Bundle\Entity\User
     */
    public function setClanMemberships(ArrayCollection $memberships)
    {
        $this->clanMemberships = $memberships;

        return $this;
    }

    /**
     * @param integer $clanid
     *
     * @return CCM\Bundle\Entity\User
     */
    public function addClanMembership(ClanMember $membership)
    {
        $this->clanMemberships->add($membership);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getClanMemberships()
    {
        return $this->clanMemberships;
    }

    /**
     * @return CCM\Bundle\Entity\User
     */
    public function eraseCredentials()
    {
        return $this;
    }

    /**
     * Determines if the given role has been granted
     *
     * @param string $role The role
     *
     * @return boolean
     */
    public function isGranted($role)
    {
        return $this->hasRole($role);
    }

        public function setAdmin($bool)
    {
        $this->admin = $bool;
    }

    public function getAdmin()
    {
        return $this->admin;
    }

    public function isAdmin()
    {
        return $this->admin;
    }

    /**
    * @param string $salt
    *
    * @return CCM\Bundle\Entity\User
    */
    public function setSalt($salt)
    {
        return $this;
    }

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @param string $password 
     *
     * @return CCM\Bundle\Entity\User
     */
    public function setPassword($password)
    {
        return $this;
    }

    /**
     * @return null
     */
    public function getPassword()
    {
        return null;
    }
}

?>
