<?php
namespace CCM\Bundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AddAdminCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName("ccm:admin:add")
             ->setDescription("Flags the given user as an administrator")
             ->addArgument("steamid", InputArgument::REQUIRED, "SteamID of user to give administrator");
    }

    protected function execute(InputInterface $in, OutputInterface $out)
    {
        $sid = $in->getArgument('steamid');

        if ($sid)
        {
            $em = $this->getContainer()
                       ->get('doctrine.orm.entity_manager');

            $user_repo = $em->getRepository('CCMBundle:User');

            $user = $user_repo->findBySteamId($sid);

            if ($user)
            {
                $user->addRole("ROLE_ADMIN");
                $user->setAdmin(true);

                $em->flush();

                $out->writeln("Gave administrator rights to \"" . $sid . "\"");
            }
            else
            {
                $out->writeln("No user found matching \"" . $sid . "\"");
            }
        }
        else
        {
            $out->writeln("Usage: 'ccm:admin:add [steamid]'");
        }
    }
}
?>
