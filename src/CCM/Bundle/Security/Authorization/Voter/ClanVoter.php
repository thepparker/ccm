<?php
namespace CCM\Bundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\HttpKernel\Log\LoggerInterface;

use CCM\Bundle\Entity\Clan;

class ClanVoter implements VoterInterface
{
    private $logger;

    const OWNER_ATTRIBUTE = "CLAN_OWNER";
    const LEADER_ATTRIBUTE = "CLAN_LEADER";
    const MEMBER_ATTRIBUTE = "CLAN_MEMBER";

    protected $attributes;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;

        $this->attributes = array(self::OWNER_ATTRIBUTE, self::LEADER_ATTRIBUTE,
                                  self::MEMBER_ATTRIBUTE
                            );
    }

    public function supportsAttribute($attr)
    {
        return in_array($attr, $this->attributes);
    }

    public function supportsClass($class)
    {
        // support all token classes
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        // only vote on clan instances and supported attributes
        if (false === $object instanceof Clan)
        {
            if ($this->logger) 
                $this->logger->debug("ClanVoter: Object is not Clan, abstaining");

            return VoterInterface::ACCESS_ABSTAIN;
        }

        $user = $token->getUser();
        if (false === $user instanceof UserInterface)
        {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        // iterate over the given attributes. as soon as we find one that
        // we support and is valid, we vote to grant
        foreach ($attributes as $attr)
        {
            if (false === $this->supportsAttribute($attr))
            {
                continue;
            }

            if ($attr === self::OWNER_ATTRIBUTE && true === $object->isOwner($user))
            {
                if ($this->logger)
                    $this->logger->debug("ClanVoter: User is owner, voting to grant");

                return VoterInterface::ACCESS_GRANTED;
            }
            else if ($attr === self::LEADER_ATTRIBUTE && true === $object->isLeader($user))
            {
                if ($this->logger)
                    $this->logger->debug("ClanVoter: User is leader, voting to grant");

                return VoterInterface::ACCESS_GRANTED;
            }
            else if ($attr === self::MEMBER_ATTRIBUTE && true === $object->isMember($user))
            {
                if ($this->logger)
                    $this->logger->debug("ClanVoter: User is member, voting to grant");

                return VoterInterface::ACCESS_GRANTED;
            }
            else
            {
                return VoterInterface::ACCESS_DENIED;
            }
        }

        return VoterInterface::ACCESS_ABSTAIN;
    }
}
?>
