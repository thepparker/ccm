<?php
namespace CCM\Bundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\HttpKernel\Log\LoggerInterface;

class AdminVoter implements VoterInterface
{
    private $logger;


    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    public function supportsAttribute($attr)
    {
        return $attr === "ROLE_ADMIN";
    }

    public function supportsClass($class)
    {
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        $user = $token->getUser();

        if (false === $user instanceof UserInterface)
        {
            if ($this->logger)
            {
                $this->logger->debug("AdminVoter: User is not logged in. Abstaining");
            }

            return VoterInterface::ACCESS_ABSTAIN;
        }

        foreach ($attributes as $attr)
        {
            if (!$this->supportsAttribute($attr))
            {
                continue;
            }

            if ($user->isGranted($attr) || $user->isAdmin())
            {
                if ($this->logger)
                {
                    $this->logger->debug("AdminVoter: User is admin. Granting access");
                }

                return VoterInterface::ACCESS_GRANTED;
            }
            else
            {
                if ($this->logger)
                {
                    $this->logger->debug("AdminVoter: User is not admin. Denying");
                }

                return VoterInterface::ACCESS_DENIED;
            }
        }

        return VoterInterface::ACCESS_ABSTAIN;
    }
}
?>
