<?php
namespace CCM\Bundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\HttpKernel\Log\LoggerInterface;

use CCM\Bundle\Entity\League;

/**
 * The league voter only checks for permissions on League objects.
 * If the user is an admin of that league, or a global admin, the voter
 * votes to grant. If not, it votes to deny. This is only applicable for
 * when the LeagueController checks for access to manage the given league.
 */
class LeagueVoter implements VoterInterface
{
    private $logger;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    public function supportsAttribute($attr)
    {
        return true;
    }

    public function supportsClass($class)
    {
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        $user = $token->getUser();

        if (false === $user instanceof UserInterface || false === $object instanceof League)
        {
            if ($this->logger)
                $this->logger->debug("LeagueVoter: User is not logged in or object is not league. Abstaining");

            return VoterInterface::ACCESS_ABSTAIN;
        }

        if ($object->isAdmin($user) || $user->isAdmin())
        {
            return VoterInterface::ACCESS_GRANTED;
        }
        else
        {
            return VoterInterface::ACCESS_DENIED;
        }
    }
}
?>
