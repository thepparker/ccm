<?php
namespace CCM\Bundle\Security\User;

use Fp\OpenIdBundle\Model\UserManager;
use Fp\OpenIdBundle\Model\IdentityManagerInterface;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\InsufficientAuthenticationException;

use CCM\Bundle\Entity\OpenIDEntity;
use CCM\Bundle\Entity\User;

use CCM\Bundle\Bridge\SteamAPI\SteamAPI;

class OpenIDUserManager extends UserManager
{
    public function __construct(IdentityManagerInterface $identity_manager, EntityManager $entity_manager)
    {
        parent::__construct($identity_manager);

        $this->entity_manager = $entity_manager;
    }

    /**
     * @param string $steamurl An OpenID token, in our case it is a steam openid link
     *                         in the form http://steamcommunity.com/openid/id/<steamid>
     *                         where steamid is a 64 bit community id
     *
     * @param array $attributes Requested attributes 
     *
     * @return CCM\Bundle\Entity\User
     */
    public function createUserFromIdentity($steamurl, array $attributes = array())
    {
        // we want to create an identity for this user which is stored in
        // the openid bundle and passed around. if the user exists in the
        // database, we set the identity's user to that. if not, we need
        // to create the user and set the identity's user

        $identity = new OpenIDEntity();

        /* regex match the URL to get our ID */
        $pattern = '/http:\/\/steamcommunity.com\/openid\/id\/([0-9]+)/';

        if (preg_match($pattern, $steamurl, $matches))
        {
            $steamid = $matches[1];
        }
        else
        {
            throw new BadCredentialsException("Unable to get SteamID from OpenID URL");
        }

        $identity->setIdentity($steamurl);
        
        $user = $this->entity_manager
                     ->getRepository("CCMBundle:User")
                     ->findBySteamId($steamid);

        // get user name via the steam api
        $sapi = new SteamAPI();
        $name = $sapi->performSingleNameCheck($steamid);

        if ($user)
        {
            // update the user's name
            $user->setNickname($name);

            $identity->setUser($user);
        }
        else
        {
            $user = new User();
            $user->setSteamid($steamid);
            $user->setNickname($name);

            // track our users with the doctrine entity manager
            $this->entity_manager->persist($user);
            
            $identity->setUser($user);
        }

        // we need to make sure the doctrine entity manager is keeping track
        // of our openid identities too
        $this->entity_manager->persist($identity);

        // flush new entities/changes to the database
        $this->entity_manager->flush();

        return $user;
    }
}

?>
