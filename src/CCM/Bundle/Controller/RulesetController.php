<?php
namespace CCM\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use CCM\Bundle\Model\BaseController as Controller;

use CCM\Bundle\Form\Type\CreateRulesetForm;
use CCM\Bundle\Entity\Form\CreateRulesetFormEntity;

use CCM\Bundle\Entity\Ruleset;

class RulesetController extends Controller
{
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('CCMBundle:Ruleset');

        $rulesets = $repo->findAll();

        if (!$rulesets)
        {
            return $this->renderError("No rulesets exist");
        }

        return $this->render('CCMBundle:Ruleset:rulesets.html.twig',
                             array("rulesets" => $rulesets)
                        );
    }

    public function createAction(Request $request)
    {
        //creating a ruleset
        $sec_context = $this->get('security.context');

        if (false === $sec_context->isGranted('ROLE_ADMIN'))
        {
            return $this->renderError("You do not have permission to create rulesets");
        }

        $doc = $this->getDoctrine();

        $ruleset_form = new CreateRulesetForm();
        $ruleset_entity = new CreateRulesetFormEntity();

        $form = $this->createForm($ruleset_form, $ruleset_entity);

        $form->handleRequest($request);

        if ($form->isValid())
        {
            $ruleset = new Ruleset();

            $ruleset->setName($ruleset_entity->getName());
            $ruleset->setValue($ruleset_entity->getValue());

            $em = $doc->getManager();
            $em->persist($ruleset);
            $em->flush();

            return $this->renderSuccess("Ruleset has been created");
        }
        else
        {
            return $this->render('CCMBundle:Ruleset:create.html.twig',
                                 array("form" => $form->createView())
                            );
        }

    }
}
?>
