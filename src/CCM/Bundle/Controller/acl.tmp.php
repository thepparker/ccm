// add the ACL for this clan
                $acl_provider = $this->get('security.acl.provider');
                $obj_identity = ObjectIdentity::fromDomainObject($clan);
                $acl = $acl_provider->createAcl($obj_identity);

                // the ACL has been created for this clan, now we need to add
                // the current user to the list as the owner
                $security_identity = UserSecurityIdentity::fromAccount($user);

                $acl->insertObjectAce($security_identity, MaskBuilder::MASK_OWNER);

                // now update the acl table. user is now the owner of this clan
                $acl_provider->updateAcl($acl);
