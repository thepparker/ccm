<?php
namespace CCM\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use CCM\Bundle\Model\BaseController as Controller;

use CCM\Bundle\Form\Type\CreateLeagueForm;
use CCM\Bundle\Entity\Form\CreateLeagueFormEntity;
use CCM\Bundle\Entity\League;
use CCM\Bundle\Entity\Division;

class LeagueController extends Controller
{
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getRepository('CCMBundle:League');

        $leagues = $repo->findAll();

        if (!$leagues)
        {
            return $this->renderError("Unable to get league information");
        }

        return $this->render('CCMBundle:League:leagues.html.twig',
                             array("leagues" => $leagues)
                        );
    }

    public function createAction(Request $request)
    {
        $sec_context = $this->get('security.context');

        if (false === $sec_context->isGranted('ROLE_ADMIN'))
        {
            return $this->renderError("You do not have permission to create leagues");
        }

        $doc = $this->getDoctrine();

        $ruleset_repo = $doc->getRepository('CCMBundle:Ruleset');
        $rulesets = $ruleset_repo->findAll();

        if (empty($rulesets))
        {
            return $this->renderError("No rulesets exist. You must create a ruleset before creating leagues");
        }

        $league_form = new CreateLeagueForm($rulesets);
        $formentity = new CreateLeagueFormEntity();

        $form = $this->createForm($league_form, $formentity);

        $form->handleRequest($request);

        if ($form->isValid())
        {
            $user = $sec_context->getToken()->getUser();
            $em = $doc->getManager();

            var_dump($formentity);

            $league = new League();

            $league->setName($formentity->getName());
            $league->setRuleset($formentity->getRuleset());


            // admins will be by nickname, so we need to convert them to ids
            $admin_names = $formentity->getAdmins()->toArray();

            // get User objects based on name
            $users = $doc->getRepository('CCMBundle:User')
                         ->findManyByName($admin_names);

            foreach ($users as $u)
            {
                $league->addAdmin($u->getId());
            }

            // persist and flush the league so the league object now has an id
            $em->persist($league);
            $em->flush();

            // the form will have an array of division form entities. we need
            // to convert them to persistant entities
            $divisions = array();

            $form_div_ents = $formentity->getDivisions()->toArray();
            foreach ($form_div_ents as $div)
            {
                // each div is a CCM\Bundle\Entity\Form\CreateDivisionEntity
                
                // we inherit admins from the league admin entry, so we need to
                // make sure there's no duplicates
                $div_admins = array();
                foreach($div->getAdmins()->toArray() as $admin)
                {
                    if (!in_array($admin, $admin_names))
                        $div_admins[] = $admin;
                }

                // get user entities
                $users = $doc->getRepository('CCMBundle:User')
                             ->findManyByName($div_admins);

                $division = new Division();

                $division->setName($div->getName())
                         ->setLeague($league->getId());

                // add admins
                foreach ($users as $u)
                {
                    $division->addAdmin($u->getId());
                }

                // now we need to add the clans, if there are any
                $clans = $doc->getRepository('CCMBundle:Clan')
                             ->findManyByName($div->getClans()->toArray());

                foreach ($clans as $clan)
                {
                    $division->addClan($clan->getId());
                }

                // persist the new division and add it to our div array
                $em->persist($division);
                $divisions[] = $division;
            }

            //flush the divisions, so all the divs now have ids
            $em->flush();

            // now update the league with the divs
            foreach ($divisions as $div)
            {
                $league->addDivision($div->getId());
            }

            // and finally, flush the league with the updated div ids
            $em->flush();
            

            return $this->renderSuccess("League has been created");
        }
        else
        {
            return $this->render('CCMBundle:League:form.html.twig',
                                 array("form" => $form->createView())
                            );
        }
    }

    public function manageListAction()
    {
        
    }

    /**
     * Serves a form of the given league, which contains the currently 
     * stored information, and allows it to be edited.
     *
     * @param Request $request The current request
     * @param League $league League ID to be modified
     */
    public function manageAction(Request $request, League $league)
    {
        $sec_context = $this->get('security.context');
        
        if (false === $sec_context->isGranted('EDIT', $league))
        {
            return $this->renderError("You do not have permission to edit this league");
        }

        $doc = $this->getDoctrine();

        $rulesets = $doc->getRepository('CCMBundle:Ruleset')
                        ->findAll();

        $league_form = new CreateLeagueForm($rulesets);
        $formentity = new CreateLeagueFormEntity();

        // set the current values of the league in the form entity
        $formentity->setName($league->getName());
        $formentity->setRuleset($league->getRuleset());

        // get the league admins and add them by name to the entity's array
        $users = $doc->getRepository('CCMBundle:User')
                     ->findManyById($league->getAdmins());

        foreach ($users as $u)
        {
            $formentity->addAdmin($u->getNickname());
        }

        // now we need to do the same for divisions
        $divisions = $doc->getRepository('CCMBundle:Division')
                         ->findManyById($league->getDivisions());
        foreach ($divisions as $div)
        {
            $formentity->addDivision($div->getName());
        }

        // now serve the form and handle that shiz
        $form = $this->createForm($league_form, $formentity);

        // populate the form with the request params
        $form->handleRequest($request);

        if ($form->isValid())
        {
            // update the league entity and flush it
            $league->setName($formentity->getName());
            $league->setRuleset($formentity->getRuleset());

        }
        else
        {
            return $this->render('CCMBundle:League:form.html.twig',
                                 array("form" => $form->createView())
                            );
        }
    }
}
?>
