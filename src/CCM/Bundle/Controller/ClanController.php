<?php
namespace CCM\Bundle\Controller;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use CCM\Bundle\Model\BaseController as Controller;
use CCM\Bundle\Entity\Form\ClanCreateFormEntity;
use CCM\Bundle\Form\Type\CreateClanForm;
use CCM\Bundle\Entity\Clan;

use CCM\Bundle\Bridge\ClanManager\Exception\ClanExistsException;

class ClanController extends Controller
{
    public function indexAction()
    {
        //$clanManager = $this->get('ccm.bundle.clans.manager');

        // get all clans
        $clanrepo = $this->getDoctrine()
                         ->getRepository('CCMBundle:Clan');

        $clans = $clanrepo->findAll();

        //pass clans to the page, which will be iterated over by twig
        return $this->render('CCMBundle:Clan:clans.html.twig', 
                       array("clans" => $clans)
                    );
    }

    /**
     * Create is never reached unless a user is logged in, as per the firewall
     */
    public function createAction(Request $request)
    {
        $num_clans = sizeof($this->getUser()->getClans());
        if ($num_clans >= 10)
        {
            $message = "You are already in 10 clans, you cannot create or join another";

            return $this->render('CCMBundle:Info:error.html.twig',
                                 array("message" => $message)
                            );
        }

        // serve the creation page
        $formentity = new ClanCreateFormEntity();
        
        $form = $this->createForm(new CreateClanForm(), $formentity);

        $form->handleRequest($request);

        if ($form->isValid())
        {
            try
            {
                $clanManager = $this->get('ccm.bundle.clans.manager');
                $user = $this->get('security.context')
                             ->getToken()
                             ->getUser();

                $clanname = $formentity->getName();
                $tag = $formentity->getTag();

                $clan = $clanManager->createClan($clanname, $tag, $user);

                $message = sprintf("Clan \"%s\" has been created!", $clanname);
                return $this->render('CCMBundle:Clan:created.html.twig',
                                      array(
                                            "message" => $message,
                                            "id" => $clan->getId()
                                    )
                                );
            }
            catch (ClanExistsException $e)
            {
                // clan with name or tag already exists, render page with error
                $invalid = array($e->getMessage());

                return $this->render('CCMBundle:Clan:create.html.twig', 
                                     array("form" => $form->createView(),
                                           "invalid" => $invalid
                                        )
                                );
            }
        }
        else
        {
            return $this->render('CCMBundle:Clan:create.html.twig', 
                                  array("form" => $form->createView())
                                );
        }
    }

    /**
     * @param string $clanid
     */
    public function clanProfileAction($clanid)
    {
        $clanid = intval($clanid);
        $clan = null;

        if ($clanid !== 0)
        {
            $clan = $this->getDoctrine()
                         ->getRepository('CCMBundle:Clan')
                         ->findOneById($clanid);
        }

        return $this->render('CCMBundle:Clan:profile.html.twig',
                             array("clan" => $clan)
                        );
    }

    public function clanManageListAction()
    {
        // use the clan manager service to get the clans this user is a
        // leader of
        $user = $this->getUser();

        $clanManager = $this->get('ccm.bundle.clans.manager');

        $leaderclans = $clanManager->getUserLeaderClans($user);

        return $this->render('CCMBundle:Clan:manage_list.html.twig',
                             array("clans" => $leaderclans)
                        );
    }

    /**
     * $clan is actually an integer clan id. the framework will automatically
     * get a ghost entity, so we'll need to get the actual entity later
     *
     * @param CCM\Bundle\Entity\Clan $clan
     */
    public function clanManageAction(Clan $clan)
    {  
        $sec_context = $this->get('security.context');

        if (!$clan)
        {
            return $this->renderError("Clan does not exist");
        }

        if (false === $sec_context->isGranted('CLAN_MEMBER', $clan))
        {
            return $this->renderError("You do not have permission to edit this clan");
        }

        /*$clan = $this->getDoctrine()
             ->getRepository('CCMBundle:Clan')
             ->findOneById($clanid);*/

        return $this->render('CCMBundle:Clan:manage.html.twig',
                             array("clan" => $clan)
                        );        
    }
}

?>
