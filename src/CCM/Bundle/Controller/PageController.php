<?php
namespace CCM\Bundle\Controller;


use CCM\Bundle\Model\BaseController as Controller;


class PageController extends Controller
{
    public function indexAction()
    {
        return $this->render('CCMBundle:Page:index.html.twig');
    }

    public function profileAction($userid = null)
    {
        if ($userid)
        {
            $user = $this->getDoctrine()
                         ->getRepository('CCMBundle:User')
                         ->findById($userid);
        }
        else
        {
            $user = $this->getUser();
        }

        return $this->render('CCMBundle:Page:profile.html.twig', 
                             array("user" => $user));
    }
}

?>
