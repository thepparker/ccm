<?php
namespace CCM\Bundle\Controller;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\DependencyInjection\ContainerAware;;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class OpenIDController extends ContainerAware
{  
    public function loginAction(Request $request)
    {   
        if ($this->userAuthed())
        {
            // user is already authed; redirect them back to the home page
            return new RedirectResponse($this->container
                                             ->get('router')
                                             ->generate('CCMBundle_home')
                                    );
        }

        // because of the way the openid bundle works (it only hooks on a form
        // submission to /login_validate), we redirect with a dummy variable
        // which will force the framework to redirect to the steam openid
        // page

        $nexturl = $this->container
                        ->get('router')
                        ->generate('CCMBundle_login_validate');

        //bypass the bundle's bullshit and make a login attempt from this url
        //we make sure it returns to validate in either case
        if ($this->container->has('ccm.bundle.openid.steam_openid'))
        {
            $relying_party = $this->container->get('ccm.bundle.openid.steam_openid');

            $nexturl = 'http://' . $request->getHttpHost() . $nexturl;

            return $relying_party->makeAuthAttempt($request, $nexturl);
        }
        else
        {
            return new RedirectResponse($nexturl, array("openid" => ""));
        }
    }

    public function validateAction(Request $request)
    {
        if ($this->userAuthed())
        {
            return new RedirectResponse($this->container
                                             ->get('router')
                                             ->generate('CCMBundle_home')
                                    );
        }

        // if we've reached this page and it wasn't hooked by the openid
        // bundle, someone tried to access it directly. therefore, we 
        // redirect them to the login page which will force a login
        return new RedirectResponse($this->container
                                    ->get('router')
                                    ->generate('CCMBundle_login')
                                );
    }

    public function loginFailAction(Request $request)
    {
        // this function is called when login fails for whatever reason

        // request is a normal httpfoundation request
        $request = $this->container->get('request');

        // get the user session
        $session = $request->getSession();

        // check if this is a return to the login page after an error
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR))
        {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        else if ($session !== null && $session->has(SecurityContext::AUTHENTICATION_ERROR))
        {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        else
        {
            $error = '';
        }


        // if we had an error, get the message and print an error message
        if ($error)
        {
            $error = $error->getMessage();
        }

        return $this->container->get('templating')->renderResponse(
            'CCMBundle:Info:error.html.twig', 
            array("error" => $error)
        );
    }


    public function logoutAction()
    {
        // if we hit this part of the method (it wasn't intercepted by the
        // security), just redirect to home
        return new RedirectResponse($this->container
                                    ->get('router')
                                    ->generate('CCMBundle_home', array())
                                );
    }

    /**
     * Determines if a user is authenticated via OpenID or not
     *
     * @return boolean
     */
    protected function userAuthed()
    {
        $authed = $this->container
                       ->get('security.context')
                       ->isGranted('IS_AUTHENTICATED_OPENID');

        return $authed;
    }
}

?>
