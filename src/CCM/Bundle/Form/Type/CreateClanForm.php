<?php
namespace CCM\Bundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateClanForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text')
                ->add('tag', 'text')
                ->add('prefix', 'text')
                ->add('suffix', 'text')
                ->add('create', 'submit');
    }

    public function getName()
    {
        return "create_clan";
    }
}
?>
