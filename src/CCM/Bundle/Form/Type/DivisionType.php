<?php
namespace CCM\Bundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DivisionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text')
                ->add('clans',
                      'bootstrap_collection', 
                      array(
                            'allow_add' => true,
                            'allow_delete' => true,
                            'add_button_text' => "Add",
                            'delete_button_text' => "Delete"
                        )
                    )
                ->add('admins',
                      'bootstrap_collection', 
                      array(
                            'allow_add' => true,
                            'allow_delete' => true,
                            'add_button_text' => "Add",
                            'delete_button_text' => "Delete"
                        )
                    );

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array("data_class" => 'CCM\Bundle\Entity\Form\CreateDivisionEntity'));
    }

    public function getName()
    {
        return 'division';
    }
}
?>
