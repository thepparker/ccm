<?php
namespace CCM\Bundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateLeagueForm extends AbstractType
{
    protected $rulesets;

    public function __construct(array $rulesets)
    {
        $this->rulesets = $rulesets;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text')
                ->add('ruleset', 
                      'choice',
                      array(
                            'choices' => $this->getRulesetChoices(),
                            'required' => true,
                            'empty_value' => "Choose a ruleset"
                        )
                    )
                ->add('admins',
                      'bootstrap_collection', 
                      array(
                            'allow_add' => true,
                            'allow_delete' => true,
                            'add_button_text' => "Add",
                            'delete_button_text' => "Delete",
                            'attr' => array(
                                            'help_text' => "League admins are considered admins of all divisions"
                                        )
                        )
                    )
                ->add('divisions',
                      'bootstrap_collection', 
                      array(
                            'allow_add' => true,
                            'allow_delete' => true,
                            'add_button_text' => "Add",
                            'delete_button_text' => "Delete",
                            'type' => new DivisionType()
                        )
                    )
                ->add('create', 'submit');
    }

    private function getRulesetChoices()
    {
        $choices = array();

        foreach ($this->rulesets as $ruleset)
        {
            $choices[$ruleset->getId()] = $ruleset->getName();
        }

        return $choices;
    }

    public function getName()
    {
        return "create_league";
    }
}
?>
