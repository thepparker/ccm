<?php
namespace CCM\Bundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateRulesetForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text')
                ->add('value', 'textarea')
                ->add('create', 'submit');
    }


    public function getName()
    {
        return "create_ruleset";
    }
}
?>
