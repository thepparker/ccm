<?php
namespace CCM\Bundle\Bridge\SteamAPI;

class SteamAPI
{
    const API_KEY = "7CD8EC56801BD2F23A1A4184A1348ADD";

    const USER_SUMMARY_INTERFACE = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/";

    /**
     * @var array An array of steamids to be checked for names
     */
    protected $nameQueue;

    public function __construct()
    {
        $this->nameQueue = array();
    }

    /**
     * This method adds the given steamid to an array of IDs that will have
     * name checks performed on the next call of the name checking cronjob
     *
     * @param bigint $steamid User steamid
     */
    public function queueForNameCheck($steamid)
    {
        // just append to the array if not already in
        if (!in_array($steamid, $this->nameQueue))
        {
            $this->nameQueue[] = $steamid;
        }
    }

    /**
     * Performs a name check on the first 100 steamids in the queue
     *
     * @return array An array of names corresponding to steamids
     */
    public function performMultiNameCheck()
    {
        // we only take the first 100 because the API will only take
        // 100 ids at a time. $nameQueue will become the array with its
        // first 100 elements removed
        $first = array_splice($this->nameQueue, 0, 100);

        // steamids must be a comma delimited string
        $steamids = implode(",", $first);

        $params = array("steamids" => $steamids);

        $url = $this->constructUrl(self::USER_SUMMARY_INTERFACE, $params);

        $result = $this->getData($url);

        $return = array();

        if ($result)
        {
            // $players is an index based array, so we can iterate over it
            // easily with a foreach
            $players = $result->response->players;
            if (sizeof($players) > 0)
            {
                foreach ($players as $player)
                {
                    $return[$player->steamid] = $player->personaname;
                }
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    /**
     * Performs a name check for the single steamid given
     *
     * @param bigint $steamid User steamid
     *
     * @return string name
     */
    public function performSingleNameCheck($steamid)
    {
        $params = array("steamids" => $steamid);

        $api_url = $this->constructUrl(self::USER_SUMMARY_INTERFACE, $params);

        $result = $this->getData($api_url);

        if ($result)
        {
            // we will only ever have a single player at most with this method
            // so we get the 0th element of players only, if there was a player
            // returned
            $players = $result->response->players;
            if (sizeof($players) > 0)
            {
                return $players[0]->personaname;
            }
            else
            {
                return "CANNOT_GET_NAME";
            }
        }
        else
        {
            return "STEAMAPI_GONE_BAD";
        }
    }

    /**
     * Performs a lookup on the given API url. *** REQUIRES CURL ***
     *
     * @param string $url The api url
     *
     * @return array The resulting array from decoding the json
     */
    protected function getData($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);

        return json_decode($response);
    }

    /**
     * Constructs a steam API url based on the given interface and params
     *
     * @param string $iface The interface URL
     * @param array $params Array of parameters to be appended to the interface
     *                      in the form of key => value
     *
     * @return string The API url
     */
    protected function constructUrl($iface, array $params)
    {
        $url = sprintf("%s?key=%s&%s&format=json", $iface, self::API_KEY, http_build_query($params, '', '&'));


        return $url;
    }
}
?>
