<?php
namespace CCM\Bundle\Bridge\RelyingParty;

use Fp\OpenIdBundle\Bridge\RelyingParty\LightOpenIdRelyingParty;
use Fp\OpenIdBundle\RelyingParty\IdentityProviderResponse;

use Fp\OpenIdBundle\RelyingParty\Exception\OpenIdAuthenticationCanceledException;
use Fp\OpenIdBundle\RelyingParty\Exception\OpenIdAuthenticationValidationFailedException;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SteamOpenIDRelyingParty extends LightOpenIdRelyingParty
{

    protected $provider_url;

    public function __construct($provider_url)
    {
        $this->provider_url = $provider_url;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return string 
     */
    protected function guessIdentifier(Request $request)
    {
        return $this->provider_url;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string $return_url
     *
     * @return Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function makeAuthAttempt(Request $request, $return_url)
    {
        // make an authentication attempt from /login
        $loid = $this->createLightOpenID($this->guessTrustRoot($request));

        $loid->identity = $this->guessIdentifier($request);
        $loid->returnUrl = $return_url;

        return new RedirectResponse($loid->authUrl());
    }
}

?>
