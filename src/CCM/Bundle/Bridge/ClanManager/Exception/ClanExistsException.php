<?php


/*
Exception for the ClanManager, called when creating a clan and a clan with
that name already exists
*/

namespace CCM\Bundle\Bridge\ClanManager\Exception;

class ClanExistsException extends \RuntimeException implements ExceptionInterface
{
    
}

?>
