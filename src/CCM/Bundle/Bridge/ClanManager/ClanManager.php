<?php
namespace CCM\Bundle\Bridge\ClanManager;

use Doctrine\ORM\EntityManager;

use CCM\Bundle\Bridge\ClanManager\Exception\ClanExistsException;

use CCM\Bundle\Entity\Clan;
use CCM\Bundle\Entity\User;

class ClanManager
{
    protected $entity_manager;

    public function __construct(EntityManager $entity_manager)
    {
        $this->entity_manager = $entity_manager;
    }

    /**
     * @param integer $clanid
     * @param User $user
     *
     * @return Clan
     */
    public function makeOwner($clanid, User $user)
    {
        $clan = $this->entity_manager->getRepository('CCMBundle:Clan')
                                     ->findOneById($clanid);

        if ($clan)
        {
            $clan->addLeader($user->getId());
            $clan->setOwner($user->getId());
        }

        return $clan;
    }

    /**
     * @param string $clanname
     * @param string $clantag
     * @param CCM\Bundle\Entity\User $user
     *
     * @throws ClanExistsException When a clan with the name or tag exists
     *
     * @return CCM\Bundle\Entity\Clan
     */
    public function createClan($clanname, $clantag, User $user)
    {
        $clan = $this->entity_manager->getRepository('CCMBundle:Clan')
                                     ->findByNameOrTag($clanname, $clantag);

        if ($clan)
        {
            throw new ClanExistsException(sprintf("Clan with name \"%s\" exists", $clanname));
        }

        // make new clan!
        $clan = new Clan();

        $uid = $user->getId();

        // set details
        $clan->setName($clanname);
        $clan->setTag($clantag);

        $clan->addMember($uid);
        $clan->addLeader($uid);
        $clan->setOwner($uid);

        // push clan data to the db and get a new id
        $this->entity_manager->persist($clan);
        $this->entity_manager->flush();

        // get the id and update the user
        $cid = $clan->getId();
        $user->addClan($cid);
        $user->addRole('ROLE_CLAN_LEADER');

        // flush the user change to the database
        $this->entity_manager->flush();

        return $clan;
    }

    /**
     * @param integer $clanid
     *
     *
     */
    public function deleteClan($clanid)
    {
        $clan = $this->entity_manager->getRepository('CCMBundle:Clan')
                                     ->findOneById($clanid);

        if ($clan)
        {
            $this->entity_manager->remove($clan);
            $this->entity_manager->flush();
        }
    }

    /**
     * @param User $user
     *
     * @return array An array of \Clan where user is a leader
     */
    public function getUserLeaderClans(User $user)
    {
        // get the user's clans
        $clanids = $user->getClans();

        // get the clan repo
        $clanrepo = $this->entity_manager
                         ->getRepository('CCMBundle:Clan');

        $clans = $clanrepo->findManyByIds($clanids);

        $leaderclans = array();
        
        // $clans is now an array of Clan objects. iterate over them and see
        // which ones our user is a leader of
        foreach ($clans as $clan)
        {
            if ($clan->isLeader($user))
            {
                $leaderclans[] = $clan;
            }
        }

        return $leaderclans;
    }
}

?>
